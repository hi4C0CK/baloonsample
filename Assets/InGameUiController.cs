﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUiController : MonoBehaviour {

    public Text scoreValue;
    public Text timeLeftText;
    int scoreCounter = 0;
    public void init()
    {
        scoreCounter = 0;
        scoreValue.text = scoreCounter.ToString();
    }


    public void scoreAdded(int addscore)
    {
        scoreCounter += addscore;
        scoreValue.text = scoreCounter.ToString();
    }

    public void OnTimeLeft(float timeLeft)
    {
        timeLeftText.text = string.Format("{0:00.0}", timeLeft);
    }



}
