﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.pool
{
    public class BaloonsPoolEmitterConfig : MonoBehaviour,IEmitterConfig
    {

        [Range(.5f, 2f)]
        public float emitionDensity;

        public AnimationCurve emitionDensityBoostEvalution= AnimationCurve.Linear(0, 0, 1, .5f);

        public float BoostAdjustedEmitionDensity(float progress)
        {
            return emitionDensity + emitionDensityBoostEvalution.Evaluate(progress);
        }

        [Range(0.1f, .5f)]
        public float minSpawnTime=0.18f;

        [Range(1, 20)]
        public int poolPreload=10;



        [Range(1f, 5f)]
        public float baloonSpeedMultiplier= 1f;

        public AnimationCurve baloonSpeed = AnimationCurve.Linear(0, 0.2f, 1, 1f);

        public float GetSpeed(float rnd) { return baloonSpeed.Evaluate(rnd) * baloonSpeedMultiplier; }

        [Range(1f, 2f)]
        public float baloonSizeMultiplier = 1f;

        public AnimationCurve baloonSizePerSpeed = AnimationCurve.Linear(0, 3, 1, 1);
        public float GetSize(float speedRnd) { return baloonSizePerSpeed.Evaluate(speedRnd) * baloonSizeMultiplier; }


        public AnimationCurve timeSpeedBoostEvalution = AnimationCurve.Linear(0, 0, 1, 2);

        public List<Sprite> baloonSprites;

        public String GetRandomSprite
        {
            get{ return baloonSprites[UnityEngine.Random.Range(0, baloonSprites.Count)].name; }
        }

        public float MinSpawnTime()
        {
            return minSpawnTime;
        }

        public int PreloadObjectsCount()
        {
            return poolPreload;
        }
    }
}
