﻿using Assets.domain;
using Assets.domain.baloon;
using Assets.pool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(ObjectsEmitter<SpawnableObject>))]
public class PoolHolder : MonoBehaviour
{

    public LinkedList<SpawnableObject> baloons = new LinkedList<SpawnableObject>();
    public BaloonObject baloonPrefab;
    //    SpawnableObjectConfig baloonConfig=new SpawnableObjectConfig(new BaloonObject());
    public BaloonsPoolEmitterConfig baloonEmitterConfig;
    public EventsObserver observer;

    ObjectsEmitter<SpawnableObject, SpawnableObjectConfig> baloonsEmitter;

    public void Awake()
    {
        init();
    }

    void init()
    {
//        baloonTempConfig.ApplyEmitterConfig(baloonEmitterConfig, 0f);
        baloonsEmitter = new ObjectsEmitter<SpawnableObject, SpawnableObjectConfig>(
            baloonPrefab
            , new SpawnableObjectConfig(new BaloonView(observer.GetPointsCollectAction))
            );
    }


    public void ResetObjects()
    {
        foreach (MonoBehaviour obj in baloons)
        {
            obj.gameObject.SetActive(false);
        }
    }

    public void EmitBaloon(float progress)
    {
        var baloon = baloons.First;
        while (baloon != null && baloon.Value.gameObject.activeSelf)
        {
            baloon = baloon.Next;
        }

        if (baloon == null)
        {
            var newBaloon = baloonsEmitter.EmitObject();
            newBaloon.ApplyConfig(new SpawnableObjectConfig(
                new BaloonView(observer.GetPointsCollectAction)
                ,baloonEmitterConfig
                , progress)); //init baloon
            baloons.AddLast(newBaloon);
        }
        else
        {
            baloon.Value.ApplyConfig(new SpawnableObjectConfig(
                new BaloonView(observer.GetPointsCollectAction)
                , baloonEmitterConfig
                , progress)); //init baloon
            baloon.Value.gameObject.SetActive(true);
        }

    }

}
