﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.pool
{
    interface IEmitterConfig
    {
        float MinSpawnTime();
        int PreloadObjectsCount();
    }
}
