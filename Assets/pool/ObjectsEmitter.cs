﻿using Assets.domain;
using Assets.domain.baloon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.pool
{
    [Serializable]
    public class ObjectsEmitter<T,C> 
        where T : SpawnableObject 
        where C : SpawnableObjectConfig
    {
        T prefab;
        C config;
        

        public ObjectsEmitter(T prefab, C config)
        {
            this.prefab = prefab;
            this.config = config;
        }

        public T EmitObject()
        {
            T go = GameObject.Instantiate(prefab) as T;
//            go.SetObject(new SpawnableObjectConfig(new BaloonView()));
            go.gameObject.SetActive(false);
            return go;
        }
    }
}
