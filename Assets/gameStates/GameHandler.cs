﻿using Assets.gamePrefs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    public GameConfig config;
    public GameRound gameRound;

    public UserMetrics userMetrics;
    public EventsObserver observer;
    

    public void Start()
    {
        Application.targetFrameRate = 60;
        gameRound.initRound(config.baloonPoolConfig,config.roundTime,RoundState.Inactive);
        gameRound.CloseGame();
        gameRound.OnRoundStateChanged += observer.GetRoundStateCollectAction;
    }

    public void StartRound()
    {
        gameRound.initRound(config.baloonPoolConfig, config.roundTime, RoundState.Ready);
        observer.SubscriberCollector(userMetrics.AddPoints);
        userMetrics.OnStartGame();
    }



    public void ExitRound()
    {
        gameRound.initRound(config.baloonPoolConfig, config.roundTime, RoundState.Inactive);
        observer.UnSubscriberCollector(userMetrics.AddPoints);
        userMetrics.OnEndGame();
    }

}
