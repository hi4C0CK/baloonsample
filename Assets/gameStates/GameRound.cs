﻿using Assets.pool;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRound : MonoBehaviour
{
    public PoolHolder objectPool;
    public GameHandler gameHandler;
    public EventsObserver observer;
    BaloonsPoolEmitterConfig emiterConfig;
    float roundCurrentTime = 0;
    float roundTotalTime = 0;


    float deltaFromLastBaloonSpawned = 0f;

    public float readyToGameDelay = 2f;
    float readyFlag = 0;

    public Action<RoundState> OnRoundStateChanged;

    public void initRound(BaloonsPoolEmitterConfig config, float aimTime, RoundState state)
    {
        emiterConfig = config;
        roundTotalTime = aimTime;
        roundCurrentTime = 0;
        GoState(state);
    }

    public void Awake()
    {
        GoState(RoundState.Inactive);
    }

    RoundState roundState;

    public void StartGame()
    {
        GoState(RoundState.Ready);
    }

    public void ContinueGame()
    {
        GoState(RoundState.Active);
    }

    public void CloseGame()
    {
        GoState(RoundState.Inactive);
    }

    public void PauseGame()
    {
        GoState(RoundState.Inactive);
    }

    void GoState(RoundState new_state)
    {
        deltaFromLastBaloonSpawned = 0;//TODO: только на старт игры
        roundState = new_state;
        objectPool.ResetObjects();
        if (new_state == RoundState.Ready)
            readyFlag = readyToGameDelay;
        observer.ChangeState(roundState);
    }

    public void Update()
    {
        deltaFromLastBaloonSpawned += Time.deltaTime;
        observer.RoundTimerChanged(TimeLeft);
        if (emiterConfig == null) return;
        if (roundState == RoundState.Ready)
        {
            readyFlag -= Time.deltaTime;
            if (readyFlag > 0)        
                return;
            roundCurrentTime = 0;
            GoState(RoundState.Active);
            return;
        }
        UpdateRoundTime(Time.deltaTime);
        if (IsReadyToSpawnBaloon)
        {
            deltaFromLastBaloonSpawned = 0;
            objectPool.EmitBaloon(RoundProgress);
        }
    }

    bool IsReadyToSpawnBaloon
    {
        get
        {
            var a = emiterConfig.BoostAdjustedEmitionDensity(RoundProgress);
            var b = UnityEngine.Random.Range(0f, deltaFromLastBaloonSpawned);
            return deltaFromLastBaloonSpawned < emiterConfig.minSpawnTime
                ? false
                : a<b;
        }
    }

    float RoundProgress
    {
        get
        {
            return roundTotalTime <= 0 
                ? 0 
                : (roundState == RoundState.Active 
                  ? roundCurrentTime / roundTotalTime 
                  : 0);
        }
    }

    float UpdateRoundTime(float delta)
    {
        if (roundState == RoundState.Active)
        {
            roundCurrentTime += delta;
            if (roundCurrentTime >= roundTotalTime)
                gameHandler.ExitRound(); 
        }
        observer.RoundTimerChanged(TimeLeft);
        return roundCurrentTime;
    }

    float TimeLeft
    {
        get
        {
            return roundTotalTime - roundCurrentTime;
        }
    }
}

public enum RoundState
{
    Inactive,
    Ready,
    Active
}

public enum RoundResult
{
    Won,
    Lost,
    Friendly
}
