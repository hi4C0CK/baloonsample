﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiController : MonoBehaviour
{


    public GameHandler gameHandler;

    public MenuController menuController;
    public InGameUiController ingameUi;
    public EventsObserver observer;
    public UserMetrics userMetrics;

    public void Awake()
    {
        observer.SubscriberRoundStateChanged(OnRoundStateChanged);
    }

    public void StartGame()
    {
        gameHandler.StartRound();
        ingameUi.init();
        observer.SubscriberCollector(ingameUi.scoreAdded);
        observer.SubscriberRoundTimer(ingameUi.OnTimeLeft);
        ShowGameUi();
    }

    void OnRoundStateChanged(RoundState state)
    {
        switch(state)
        {
            case RoundState.Inactive:
                ShowMenuUi();
                break;
            case RoundState.Ready:
                ShowGameUi();
                break;
            case RoundState.Active:
                ShowGameUi();
                break;
        }
    }

    public void EndGame()
    {
        gameHandler.ExitRound();
        observer.UnSubscriberCollector(ingameUi.scoreAdded);
        observer.UnSubscriberRoundTimer(ingameUi.OnTimeLeft);
        ShowMenuUi();
    }

    public void ShowGameUi()
    {
        menuController.gameObject.SetActive(false);
        ingameUi.gameObject.SetActive(true);
    }

    public void ShowMenuUi()
    {
        observer.UnSubscriberCollector(ingameUi.scoreAdded);
        observer.UnSubscriberRoundTimer(ingameUi.OnTimeLeft);
        ingameUi.gameObject.SetActive(false);
        menuController.gameObject.SetActive(true);
        menuController.SetHighScore(userMetrics.GetBestScore);
    }



}

