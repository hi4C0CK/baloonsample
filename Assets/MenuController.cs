﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

    public Text highScoreText;

    public void SetHighScore(int score)
    {
        highScoreText.text ="BEST SCORE:\n\r"+score.ToString();
    }
    
    public void QuitGame()
    {
        Application.Quit();
    }
}
