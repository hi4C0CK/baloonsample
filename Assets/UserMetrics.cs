﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserMetrics : MonoBehaviour {

    int bestScoreEver = 0;
    int currentScore = 0;

    public int GetBestScore { get { return bestScoreEver; } }
    public void OnStartGame()
    {
        currentScore = 0;
    }

    public void OnEndGame()
    {
        //
    }

    public void AddPoints(int points)
    {
        currentScore += points;
        bestScoreEver = Mathf.Max(currentScore, bestScoreEver);
    }
}
