﻿using Assets.domain.baloon;
using Assets.pool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.gamePrefs
{
    [CreateAssetMenu(fileName ="GameConfig", menuName ="ScObject/Create Game Config)")]
    public class GameConfig:ScriptableObject
    {
        [Range(5f, 60f)]
        public float roundTime = 20f;
        public BaloonsPoolEmitterConfig baloonPoolConfig;
        
    }
}
