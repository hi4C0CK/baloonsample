﻿Shader "Custom/TopCamBlessShader" {
		Properties
		{
			_MainTex("Texture", 2D) = "white" {}
		}
			SubShader
		{
			Cull Off ZWrite Off ZTest Always
			Blend One OneMinusSrcAlpha

			Pass
		{
			CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

			struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
			float2 screenPos : TEXCOORD1;
		};

		v2f vert(appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.screenPos = ComputeScreenPos(o.vertex);
			o.uv = v.uv;
			return o;
		}

		sampler2D _MainTex;

		fixed4 frag(v2f i) : SV_Target
		{
			fixed4 col = tex2D(_MainTex, i.uv);
		// just invert the colors
			col.rgb =lerp(col.rgb, 1, 1-cos(i.screenPos.y*1.5));
			col *= col.a;
		return col;
		}
			ENDCG
		}
		}
	}

