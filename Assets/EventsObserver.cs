﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsObserver : MonoBehaviour
{
    Action<int> pointsCollector;

    public Action<int> GetPointsCollectAction { get { return pointsCollector; } }

    void CollectBaloon(int points)
    {
        if (pointsCollector!=null)
            pointsCollector(points);
    }

    public void SubscriberCollector(Action<int> onBaloonCollected)
    {
        pointsCollector += onBaloonCollected;
    }

    public void UnSubscriberCollector(Action<int> onBaloonCollected)
    {
        pointsCollector -= onBaloonCollected;
    }


    Action<float> roundTimer;

    public Action<float> GetroundTimerAction { get { return roundTimer; } }

    public void RoundTimerChanged(float  time)
    {
        if (roundTimer  != null)
            roundTimer(time);
    }

    public void SubscriberRoundTimer(Action<float> onRounTimeChanged)
    {
        roundTimer += onRounTimeChanged;
    }

    public void UnSubscriberRoundTimer(Action<float> onRounTimeChanged)
    {
        roundTimer -= onRounTimeChanged;
    }


    Action<RoundState> roundStateCollector;

    public Action<RoundState> GetRoundStateCollectAction { get { return roundStateCollector; } }

    public void ChangeState(RoundState newState)
    {
        if (roundStateCollector != null)
            roundStateCollector(newState);
    }

    public void SubscriberRoundStateChanged(Action<RoundState> onRoundStateChanged)
    {
        roundStateCollector += onRoundStateChanged;
    }
    

    public void UnSubscriberRoundStateChanged(Action<RoundState> onRoundStateChanged)
    {
        roundStateCollector += onRoundStateChanged;
    }



    public void UnSubscribeAllCollectors()
    {
        pointsCollector = null;
        roundStateCollector = null;
    }

}

