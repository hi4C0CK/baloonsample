﻿using Assets.domain;
using Assets.pool;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpawnableObject : MonoBehaviour
{

    public SpawnableObjectConfig config;

    public void ApplyConfig(SpawnableObjectConfig config)
    {
        this.config = config;
        OnInit();
    }


    public void ApplyConfig(ViewObject entity, BaloonsPoolEmitterConfig emiterConfig, float progress)
    {
        if (config == null)
        {
            config = new SpawnableObjectConfig(entity);
//            throw new NullReferenceException("не задана конфигурация!");
        }
        config.ApplyEmitterConfig(emiterConfig, progress);
        OnInit();
    }

    /*
    public void OnEnable()
    {
        OnInit();
    }
    */

    protected abstract void OnInit();

    
}
