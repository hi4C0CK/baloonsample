﻿using Assets.pool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.domain
{
    [Serializable]
    public class SpawnableObjectConfig
    {
        public ViewObject entity;

        

        public SpawnableObjectConfig(ViewObject entity)
        {
            this.entity = entity;
        }

        public SpawnableObjectConfig(ViewObject entity, BaloonsPoolEmitterConfig emiterConfig, float progress)
        {
            this.entity = entity;
            ApplyEmitterConfig(emiterConfig, progress);
        }

        public void ApplyEmitterConfig(BaloonsPoolEmitterConfig emiterConfig, float progress)
        {
            float speedRnd = UnityEngine.Random.Range(0, 1f);
            entity.speed_y = emiterConfig.GetSpeed(speedRnd)
                // увеличение сукорости к концу раунда
                *(1f+5*emiterConfig.timeSpeedBoostEvalution.Evaluate(progress));
            entity.size_x = emiterConfig.GetSize(speedRnd);
            entity.collectPoints = (int)Math.Round(entity.speed_y *10f);
            entity.spriteName = emiterConfig.GetRandomSprite;
        }
    }
}
