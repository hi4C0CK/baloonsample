﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.domain.baloon
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class BaloonObject : SpawnableObject
    {
        public SpriteRenderer spriteRenderer;
        public BoxCollider2D collider;

        protected override void OnInit()
        {
            //TODO: вынести в пул мэнэджер и подгружать из ассетов
            Sprite[] sprites = Resources.LoadAll<Sprite>("cg/baloons");
            spriteRenderer.sprite = sprites.First(x => x.name.Equals(config.entity.spriteName));

            //TODO: вынести в пул мэнэджер и подгружать из конфига раунда
            transform.localScale = Vector3.one * config.entity.size_x;
            transform.localPosition = Vector3.zero + Vector3.right * UnityEngine.Random.Range(-1f, 1) * Camera.main.orthographicSize;

            collider.size = spriteRenderer.size;
        }

        public void OnMouseDown()
        {
            config.entity.OnTouch();
            gameObject.SetActive(false);
        }

        public void Update()
        {
            gameObject.transform.Translate(config.entity.speed_x * Time.deltaTime, config.entity.speed_y * Time.deltaTime, 0);

            if (!spriteRenderer.isVisible)
                gameObject.SetActive(false);
        }
    }
}
