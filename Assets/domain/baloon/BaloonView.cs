﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.domain.baloon
{
    [Serializable]
    public class BaloonView : ViewObject
    {
        Action<int> onBaloonCollected;
           
        public BaloonView(Action<int> collectEvent)
        {
            onBaloonCollected = collectEvent;
        }

        public override void Act(float deltaTime)
        {
            throw new NotImplementedException();
        }

        public override void OnSpawn()
        {
            throw new NotImplementedException();
        }

        public override void OnTouch()
        {
            if (onBaloonCollected!=null)
                onBaloonCollected(collectPoints);
//            throw new NotImplementedException();
        }
    }
}
