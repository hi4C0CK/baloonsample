﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.domain
{
    [Serializable]
    public abstract class ViewObject
    {
        public float size_x { get; set; }
        public float size_y { get; set; }

        public float speed_x { get; set; }
        public float speed_y { get; set; }

        public int collectPoints { get; set; }

        public String spriteName;
        

        public abstract void OnSpawn();
        public abstract void OnTouch();
        public abstract void Act(float deltaTime);
    }


}
